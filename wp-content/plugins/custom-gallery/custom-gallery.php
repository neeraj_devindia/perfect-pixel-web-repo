

<?php
/*
Plugin Name: Custom Gallery
Plugin URI: 
Description: Creates the Custom Gallery for each page
Author: M.Tiwari
Version: 1.0
Author URI: https://examsmyantra.com
*/


function ava_test_init() {
          wp_enqueue_script( 'scrolltotop-src', plugins_url( 'myjs.js' , __FILE__ ), array('jquery') );
}
add_action('wp_enqueue_script','ava_test_init');
function cg_create_tables() {
		global $wpdb;
   	$charset_collate = $wpdb->get_charset_collate();
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		$sql_create_table = "CREATE TABLE `".$wpdb->prefix."custom_gallery` (`cg_id` int(11) NOT NULL AUTO_INCREMENT,`cg_name` tinytext NOT NULL,`cg_image` tinytext NOT NULL,PRIMARY KEY (`cg_id`)) $charset_collate;";
		dbDelta($sql_create_table);
}

// Create tables on plugin activation
register_activation_hook( __FILE__, 'cg_create_tables' );

function cg_admin_actions(){
	add_media_page('Custom Gallery','Custom slider','administrator','cg_gallery','gallery_manager');
}

add_action('admin_menu','cg_admin_actions');

function gallery_manager(){
	include('includes/gallery-manager.php');
}

include_once('includes/shortcodes.php');


