<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'steDB');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*[5z|@2pOgCs~-V5|Nm|J 8S#nc8%2No/hoK:Z5dN;IS;5e9KsrgAP*(q^BT28}C');
define('SECURE_AUTH_KEY',  '}6G3/FNtur&+x5FgIG*vFUF~>GAk#JVMX6&2-e`ClD$<%W9,z8jWn#:Gxp|>TT:g');
define('LOGGED_IN_KEY',    '-T>]Vb ]:9jvqwO#|R|Gwz@uizD||(pT,oOQ=3(C5TDysKn@TK!_Z^94k%#,)K{4');
define('NONCE_KEY',        '7|zkwC+l][OUy`7>1ZU&*hI3KI%n.Afk<Psp$TbyGj]D9MS<oR/9|9WIL5a1+jc+');
define('AUTH_SALT',        'W3F-d~(!Helr(44$g(djJ_ylW}|o+D0`2YZ-N:!@3O0+9Jo^7 d{~rINwL#>k6uc');
define('SECURE_AUTH_SALT', 'QegOT!dtQ9nMUn`s#plnBk_(l+D=uW-XFD7wb|Kd[=mA7)U^UPxGyI%*[O=kg]|-');
define('LOGGED_IN_SALT',   '{zJuDK <u$|_0_cp[r+.LF(<}TE:[~92RX=+#(tVikAz;8%TwKrGu+u1BMkB<|:K');
define('NONCE_SALT',       'j_t5p3*CIysL{fSDT|s[{$-Qi@:sss|M.^Ps|+W37[P7[LP5 e:;GkonA-liaIBQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
